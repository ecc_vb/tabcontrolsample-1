﻿Public Class Form1

    ' Project:TabControlSample1
    ' Auther:IE3A No.20, 村田直人
    ' Date: 2016年08月1日

    'フォーム Load
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        '起動時のフォームサイズ指定
        Me.Size = New Size(500, 300)

        'テキストボックスの生成(インスタンス化)
        Dim tBox As TextBox = New TextBox()

        'テキストボックスのプロパティ設定
        SetTextBox(tBox, "TextBox1")

        'タブコントロール
        With TabControl1

            'タブページ
            With TabPage1

                'テキスト設定
                .Text = "TabPage(1)"

                '背景の設定(タブと同色に設定)
                .UseVisualStyleBackColor = True

                'テキストボックスを配置 ?
                .Controls.Add(tBox)

            End With

            'タブページ(1)のツールチップテキストの設定
            SetToolTip(.Controls(0))


        End With

        'タブページ削除ボタン →無効
        DeleteButton.Enabled = False

        '「＜」「＞」ボタン無効
        LeftButton.Enabled = False
        RightButton.Enabled = False

        'ツールバー「ボーダー」コンボボックス
        With BorderComboBox

            '[ボーダー]コンボボックスに項目追加
            For i As Integer = 0 To 20

                .Items.Add(i)
            Next

            '初期値の設定(5)
            .SelectedIndex = 5

        End With
    End Sub


    'フォームShownイベント
    Private Sub Form1_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        'TextBox1にフォーカスをうつす
        TabControl1.TabPages(0).Controls(0).Focus()
    End Sub

    '[タブページ追加]ボタン,コンテキスト追加ボタン
    Private Sub AddButton_Click(sender As Object, e As EventArgs) _
        Handles AddButton.Click, AddMenuItem.Click


        '現在のタブ数を変数に格納
        Dim pCount As Integer = TabControl1.TabCount

        'カウンタ（タブページ、テキストボックスの作成に使用する）
        Static pOrder As Integer = 1
        pOrder += 1 'すでにあるページをテキストボックス

        Dim pOrderStr As String = pOrder.ToString

        'テキストボックス作成
        Dim tBox As TextBox = New TextBox()

        'テキストボックスのプロパティ設定
        SettextBox(tBox, "TextBox" & pOrderStr)

        'タブコントロール
        With TabControl1

            'タブページの追加
            .TabPages.Add("TabPage" & pOrder, "Tabpage(" & pOrder & ")")

            '追加したタブページ
            With .TabPages(pCount)

                '背景の設定(タブと同色に設定)
                .UseVisualStyleBackColor = True

                'テキストボックスを配置
                .Controls.Add(tBox)
            End With

            'ツールチップテキストの設定（追加したタブページ）
            SetToolTip(.TabPages(pCount))

            '追加したページを選択
            .SelectedIndex = pCount
        End With

        '[ページ削除]ボタン有効
        DeleteButton.Enabled = True

    End Sub

    '[タブページ削除]ボタン,コンテキスト削除ボタン
    Private Sub DeleteButton_Click(sender As Object, e As EventArgs) _
        Handles DeleteButton.Click, DeleteMenuItem.Click


        With TabControl1

            '選択されているタブのインデックス
            Dim pNo As Integer = .SelectedIndex

            '選択されているページの削除
            .TabPages.Remove(.SelectedTab)

            If pNo = .TabCount Then

                pNo -= 1
            End If

            '削除したページの右or末尾のページを選択
            .SelectedIndex = pNo

            '削除ボタンの有効,無効設定
            DeleteButton.Enabled = .TabCount > 1

            'ツールチップテキストの再設定
            For Each tPage As TabPage In .TabPages

                SetToolTip(tPage)
            Next

        End With

    End Sub

    '一つ前のタブを選択
    Private Sub LeftButton_Click(sender As Object, e As EventArgs) _
        Handles LeftButton.Click, LeftMenuItem.Click

        '前のタブページを選択
        TabControl1.SelectedIndex -= 1
    End Sub

    '次のタブを選択
    Private Sub RightButton_Click(sender As Object, e As EventArgs) _
        Handles RightButton.Click, RightMenuItem.Click

        '次のタブページを選択
        TabControl1.SelectedIndex += 1
    End Sub


    '[ボーダー]コンボボックス
    Private Sub BorderComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles BorderComboBox.SelectedIndexChanged


        '全タブページに埋め込まれた
        'テキストボックスの幅を設定
        For Each tPage As TabPage In TabControl1.TabPages

            tPage.Padding = New Padding(BorderComboBox.SelectedIndex)
        Next

        '選択中のタブのテキストボックスにフォーカスを設定
        TabControl1.SelectedTab.Controls(0).Focus()
    End Sub

    'タブコントロール Selected Changed
    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged

        'タブコントロール
        With TabControl1

            '[<]左ページ,[>]右ページ「有効/無効」設定
            LeftButton.Enabled = .SelectedIndex > 0
            RightButton.Enabled = .SelectedIndex < .TabCount - 1

            '選択中のタブのテキストボックスにフォーカスを設定
            .SelectedTab.Controls(0).Focus()
        End With
    End Sub

    'コンテキストメニュー
    Private Sub ContextMenuStrip1_Opened(sender As Object, e As EventArgs) Handles ContextMenuStrip1.Opened

        '「タブページ削除]メニュー　「表示/非表示」
        DeleteMenuItem.Visible = DeleteButton.Enabled

        '「左ページ,右ページ]メニュー　「表示/非表示」
        LeftMenuItem.Visible = LeftButton.Enabled
        RightMenuItem.Visible = RightButton.Enabled

        '左ページ,右ページ]メニューが非表示のときセパレータも非表示
        LeftMenuSeparator.Visible = _
            LeftMenuItem.Visible And RightMenuItem.Visible

        '[ツールバー表示/非表示]メニューのテキスト設定
        If ToolStrip1.Visible Then

            'ツールバー[表示時]
            ToolBarMenuItem.Text = "ツールバー非表示(&T)"
        Else

            'ツールバー[非表示時]
            ToolBarMenuItem.Text = "ツールバー表示(&T)"
        End If

    End Sub

    '[ツールバー表示]メニューアイテム
    Private Sub ToolBarMenuItem_Click(sender As Object, e As EventArgs) Handles ToolBarMenuItem.Click

        '[ツールバー]の「表示/非表示」設定
        ToolStrip1.Visible = Not ToolStrip1.Visible
    End Sub

    'ツールチップテキスト設定　メソッド
    Private Sub SetToolTip(ByRef tPage As TabPage)

        'ツールチップテキスト設定
        tPage.ToolTipText = "Name=" & tPage.Name & _
            ", Index=" & TabControl1.Controls.IndexOf(tPage)

    End Sub


    'テキストボックス設定
    Private Sub SettextBox(ByRef tBox As TextBox, ByVal boxName As String)

        'プロパティ設定
        With tBox

            'オブジェクト名
            .Name = boxName

            'テキスト
            .Text = boxName

            '複数行入力許可
            .Multiline = True

            'タブページの四辺にドッキング
            .Dock = DockStyle.Fill

            '文字色/背景色
            .ForeColor = Color.DarkBlue
            .BackColor = Color.Lavender

            'フォント設定
            .Font = New Font("ＭＳ　明朝", 12)

            'カーソルをテキストの最後に移動
            .SelectionStart = .TextLength
        End With

    End Sub
End Class
