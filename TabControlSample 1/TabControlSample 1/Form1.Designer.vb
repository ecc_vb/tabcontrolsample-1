﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.AddButton = New System.Windows.Forms.ToolStripButton()
        Me.DeleteButtonSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.DeleteButton = New System.Windows.Forms.ToolStripButton()
        Me.LeftButtonSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.LeftButton = New System.Windows.Forms.ToolStripButton()
        Me.RightButtonSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.RightButton = New System.Windows.Forms.ToolStripButton()
        Me.BorderLabelSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BorderLabel = New System.Windows.Forms.ToolStripLabel()
        Me.BorderComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me.AddMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LeftMenuSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.LeftMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RightMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolBarMenuSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolBarMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.TabControl1)
        Me.ToolStripContainer1.ContentPanel.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.ToolStripContainer1.ContentPanel.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(673, 210)
        Me.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStripContainer1.LeftToolStripPanelVisible = False
        Me.ToolStripContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripContainer1.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.RightToolStripPanelVisible = False
        Me.ToolStripContainer1.Size = New System.Drawing.Size(673, 243)
        Me.ToolStripContainer1.TabIndex = 0
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Controls.Add(Me.ToolStrip1)
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(7, 6)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.Padding = New System.Drawing.Point(8, 3)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.ShowToolTips = True
        Me.TabControl1.Size = New System.Drawing.Size(659, 198)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Location = New System.Drawing.Point(4, 28)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.TabPage1.Size = New System.Drawing.Size(651, 166)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.CanOverflow = False
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.ToolStrip1.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddButton, Me.DeleteButtonSeparator, Me.DeleteButton, Me.LeftButtonSeparator, Me.LeftButton, Me.RightButtonSeparator, Me.RightButton, Me.BorderLabelSeparator, Me.BorderLabel, Me.BorderComboBox})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(673, 33)
        Me.ToolStrip1.Stretch = True
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'AddButton
        '
        Me.AddButton.Image = Global.TabControlSample_1.My.Resources.Resources.InsertTabControl
        Me.AddButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AddButton.Name = "AddButton"
        Me.AddButton.Size = New System.Drawing.Size(145, 30)
        Me.AddButton.Text = "タブページ追加"
        '
        'DeleteButtonSeparator
        '
        Me.DeleteButtonSeparator.Name = "DeleteButtonSeparator"
        Me.DeleteButtonSeparator.Size = New System.Drawing.Size(6, 33)
        '
        'DeleteButton
        '
        Me.DeleteButton.Image = Global.TabControlSample_1.My.Resources.Resources.BuilderDialog_delete
        Me.DeleteButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Size = New System.Drawing.Size(145, 30)
        Me.DeleteButton.Text = "タブページ削除"
        '
        'LeftButtonSeparator
        '
        Me.LeftButtonSeparator.Name = "LeftButtonSeparator"
        Me.LeftButtonSeparator.Size = New System.Drawing.Size(6, 33)
        '
        'LeftButton
        '
        Me.LeftButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.LeftButton.Image = Global.TabControlSample_1.My.Resources.Resources.DataContainer_MovePrevious
        Me.LeftButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.LeftButton.Name = "LeftButton"
        Me.LeftButton.Size = New System.Drawing.Size(28, 30)
        Me.LeftButton.Text = "ToolStripButton1"
        Me.LeftButton.ToolTipText = "左ページ"
        '
        'RightButtonSeparator
        '
        Me.RightButtonSeparator.Name = "RightButtonSeparator"
        Me.RightButtonSeparator.Size = New System.Drawing.Size(6, 33)
        '
        'RightButton
        '
        Me.RightButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RightButton.Image = Global.TabControlSample_1.My.Resources.Resources.DataContainer_MoveNext
        Me.RightButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.RightButton.Name = "RightButton"
        Me.RightButton.Size = New System.Drawing.Size(28, 30)
        Me.RightButton.Text = "ToolStripButton1"
        '
        'BorderLabelSeparator
        '
        Me.BorderLabelSeparator.Name = "BorderLabelSeparator"
        Me.BorderLabelSeparator.Size = New System.Drawing.Size(6, 33)
        '
        'BorderLabel
        '
        Me.BorderLabel.Margin = New System.Windows.Forms.Padding(10, 1, 0, 2)
        Me.BorderLabel.Name = "BorderLabel"
        Me.BorderLabel.Size = New System.Drawing.Size(76, 30)
        Me.BorderLabel.Text = "ボーダー:"
        '
        'BorderComboBox
        '
        Me.BorderComboBox.AutoSize = False
        Me.BorderComboBox.DropDownHeight = 152
        Me.BorderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.BorderComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BorderComboBox.IntegralHeight = False
        Me.BorderComboBox.Name = "BorderComboBox"
        Me.BorderComboBox.Size = New System.Drawing.Size(44, 33)
        '
        'AddMenuItem
        '
        Me.AddMenuItem.Name = "AddMenuItem"
        Me.AddMenuItem.Size = New System.Drawing.Size(208, 30)
        Me.AddMenuItem.Text = "タブページ追加(&A)"
        '
        'DeleteMenuItem
        '
        Me.DeleteMenuItem.Name = "DeleteMenuItem"
        Me.DeleteMenuItem.Size = New System.Drawing.Size(208, 30)
        Me.DeleteMenuItem.Text = "タブページ削除(&D)"
        '
        'LeftMenuSeparator
        '
        Me.LeftMenuSeparator.Name = "LeftMenuSeparator"
        Me.LeftMenuSeparator.Size = New System.Drawing.Size(205, 6)
        '
        'LeftMenuItem
        '
        Me.LeftMenuItem.Name = "LeftMenuItem"
        Me.LeftMenuItem.Size = New System.Drawing.Size(208, 30)
        Me.LeftMenuItem.Text = "左ページ(&L)"
        '
        'RightMenuItem
        '
        Me.RightMenuItem.Name = "RightMenuItem"
        Me.RightMenuItem.Size = New System.Drawing.Size(208, 30)
        Me.RightMenuItem.Text = "右ページ(&R)"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddMenuItem, Me.DeleteMenuItem, Me.LeftMenuSeparator, Me.LeftMenuItem, Me.RightMenuItem, Me.ToolBarMenuSeparator2, Me.ToolBarMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.ShowImageMargin = False
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(209, 166)
        '
        'ToolBarMenuSeparator2
        '
        Me.ToolBarMenuSeparator2.Name = "ToolBarMenuSeparator2"
        Me.ToolBarMenuSeparator2.Size = New System.Drawing.Size(205, 6)
        '
        'ToolBarMenuItem
        '
        Me.ToolBarMenuItem.Name = "ToolBarMenuItem"
        Me.ToolBarMenuItem.Size = New System.Drawing.Size(208, 30)
        Me.ToolBarMenuItem.Text = "ToolBarMenuItem"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(673, 243)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.MinimumSize = New System.Drawing.Size(685, 272)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents AddButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents DeleteButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents LeftButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents RightButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents DeleteButtonSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LeftButtonSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RightButtonSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BorderLabelSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BorderLabel As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BorderComboBox As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents AddMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LeftMenuSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LeftMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RightMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolBarMenuSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolBarMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
